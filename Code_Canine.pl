:- use_module(library(csv)).
:- dynamic fact/15.
:- dynamic counter/2.
:- dynamic counter_less/2.
:- dynamic counter_more/2.

init_database:-
    retractall(fact(_,_,_,_,_,_,_,_,_,_,_,_,_,_,_)),
    retractall(counter(_,_)),
    csv_read_file('training_adult.data', Data, [functor(fact), row_arity(15)]),
    maplist(assert, Data),

    csv_read_file('testing_adult.data', TestingData, [functor(fact_testing), row_arity(15)]),
    maplist(assert, TestingData),

    findall(('Total',Salary),
        fact(Age, 
        WorkClass, 
        FinalWeight, 
        Education, 
        EducationNumber, 
        MaritalStatus, 
        Occupation, 
        Relationship, 
        Race, 
        Sex, 
        CapitalGain, 
        CapitalLoss, 
        HoursPerWeek, 
        NativeCountry, 
        Salary),
        Total),
    count_func(Total),

    findall((Age,Salary),
    fact(Age, 
        WorkClass, 
        FinalWeight, 
        Education, 
        EducationNumber, 
        MaritalStatus, 
        Occupation, 
        Relationship, 
        Race, 
        Sex, 
        CapitalGain, 
        CapitalLoss, 
        HoursPerWeek, 
        NativeCountry, 
        Salary),
        AgeList),
    count_func_age(AgeList),

    findall((WorkClass,Salary),
    fact(Age, 
        WorkClass, 
        FinalWeight, 
        Education, 
        EducationNumber, 
        MaritalStatus, 
        Occupation, 
        Relationship, 
        Race, 
        Sex, 
        CapitalGain, 
        CapitalLoss, 
        HoursPerWeek, 
        NativeCountry, 
        Salary),
        WorkClassList),
    count_func(WorkClassList),

    findall((FinalWeight,Salary),
    fact(Age, 
        WorkClass, 
        FinalWeight, 
        Education, 
        EducationNumber, 
        MaritalStatus, 
        Occupation, 
        Relationship, 
        Race, 
        Sex, 
        CapitalGain, 
        CapitalLoss, 
        HoursPerWeek, 
        NativeCountry, 
        Salary),
        FinalWeightList),
    count_final_weight(FinalWeightList),

    findall((Education,Salary),
    fact(Age, 
        WorkClass, 
        FinalWeight, 
        Education, 
        EducationNumber, 
        MaritalStatus, 
        Occupation, 
        Relationship, 
        Race, 
        Sex, 
        CapitalGain, 
        CapitalLoss, 
        HoursPerWeek, 
        NativeCountry, 
        Salary),
        EducationList),
    count_func(EducationList),

    findall((MaritalStatus,Salary),
    fact(Age, 
        WorkClass, 
        FinalWeight, 
        Education, 
        EducationNumber, 
        MaritalStatus, 
        Occupation, 
        Relationship, 
        Race, 
        Sex, 
        CapitalGain, 
        CapitalLoss, 
        HoursPerWeek, 
        NativeCountry, 
        Salary),
        MaritalStatusList),
    count_func(MaritalStatusList),

    findall((Occupation,Salary),
    fact(Age, 
        WorkClass, 
        FinalWeight, 
        Education, 
        EducationNumber, 
        MaritalStatus, 
        Occupation, 
        Relationship, 
        Race, 
        Sex, 
        CapitalGain, 
        CapitalLoss, 
        HoursPerWeek, 
        NativeCountry, 
        Salary),
        OccupationList),
    count_func(OccupationList),

    findall((Relationship,Salary),
    fact(Age, 
        WorkClass, 
        FinalWeight, 
        Education, 
        EducationNumber, 
        MaritalStatus, 
        Occupation, 
        Relationship, 
        Race, 
        Sex, 
        CapitalGain, 
        CapitalLoss, 
        HoursPerWeek, 
        NativeCountry, 
        Salary),
        RelationshipList),
    count_func(RelationshipList),

    findall((Race,Salary),
    fact(Age, 
        WorkClass, 
        FinalWeight, 
        Education, 
        EducationNumber, 
        MaritalStatus, 
        Occupation, 
        Relationship, 
        Race, 
        Sex, 
        CapitalGain, 
        CapitalLoss, 
        HoursPerWeek, 
        NativeCountry, 
        Salary),
        RaceList),
    count_func(RaceList),

    findall((Sex,Salary),
    fact(Age, 
        WorkClass, 
        FinalWeight, 
        Education, 
        EducationNumber, 
        MaritalStatus, 
        Occupation, 
        Relationship, 
        Race, 
        Sex, 
        CapitalGain, 
        CapitalLoss, 
        HoursPerWeek, 
        NativeCountry, 
        Salary),
        SexList),
    count_func(SexList),

    findall((CapitalGain,Salary),
    fact(Age, 
        WorkClass, 
        FinalWeight, 
        Education, 
        EducationNumber, 
        MaritalStatus, 
        Occupation, 
        Relationship, 
        Race, 
        Sex, 
        CapitalGain, 
        CapitalLoss, 
        HoursPerWeek, 
        NativeCountry, 
        Salary),
        CapitalGainList),
    count_capital_gain(CapitalGainList),

    findall((CapitalLoss,Salary),
    fact(Age, 
        WorkClass, 
        FinalWeight, 
        Education, 
        EducationNumber, 
        MaritalStatus, 
        Occupation, 
        Relationship, 
        Race, 
        Sex, 
        CapitalGain, 
        CapitalLoss, 
        HoursPerWeek, 
        NativeCountry, 
        Salary),
        CapitalLossList),
    count_capital_loss(CapitalLossList),

    findall((HoursPerWeek,Salary),
    fact(Age, 
        WorkClass, 
        FinalWeight, 
        Education, 
        EducationNumber, 
        MaritalStatus, 
        Occupation, 
        Relationship, 
        Race, 
        Sex, 
        CapitalGain, 
        CapitalLoss, 
        HoursPerWeek, 
        NativeCountry, 
        Salary),
        HoursPerWeekList),
    count_func_hours_per_week(HoursPerWeekList),

    findall((NativeCountry,Salary),
    fact(Age, 
        WorkClass, 
        FinalWeight, 
        Education, 
        EducationNumber, 
        MaritalStatus, 
        Occupation, 
        Relationship, 
        Race, 
        Sex, 
        CapitalGain, 
        CapitalLoss, 
        HoursPerWeek, 
        NativeCountry, 
        Salary),
        NativeCountryList),
    count_func(NativeCountryList),
    fail.


exist_check(Data):-
    not(counter_more(Data, _)),!,
    create_assertz(Data).

exist_check(Data):-
    counter_more(Data, _),!.


create_assertz(Text):-
    assertz(counter_more(Text,0)),
    assertz(counter_less(Text,0)).
    
count_func([]):-!.
count_func([(Feature, Salary)|Next]):-
    Salary='<=50K',
    exist_check(Feature),
    counter_less(Feature, Count),
    retractall(counter_less(Feature, Count)),
    NewCount is Count+1,
    assertz(counter_less(Feature,NewCount)),
    count_func(Next).
count_func([(Feature, Salary)|Next]):-
    Salary='>50K',
    exist_check(Feature),
    counter_more(Feature, Count),
    retractall(counter_more(Feature, Count)),
    NewCount is Count+1,
    assertz(counter_more(Feature,NewCount)),
    count_func(Next).

count_func_age([]):-!.
count_func_age([(Age, Salary)|Next]):-
    Salary='<=50K',
    parse_age(Age, CategorizedAge),
    exist_check(CategorizedAge),
    counter_less(CategorizedAge, Count),
    retractall(counter_less(CategorizedAge, Count)),
    NewCount is Count+1,
    assertz(counter_less(CategorizedAge,NewCount)),
    count_func_age(Next).
count_func_age([(Age, Salary)|Next]):-
    Salary='>50K',
    parse_age(Age, CategorizedAge),
    exist_check(CategorizedAge),
    counter_more(CategorizedAge, Count),
    retractall(counter_more(CategorizedAge, Count)),
    NewCount is Count+1,
    assertz(counter_more(CategorizedAge,NewCount)),
    count_func_age(Next).

parse_age(Age, 'Young Adult'):-
    Age<30,!.
parse_age(Age, 'Adult'):-
    Age<40,!.
parse_age(Age, 'Middle Age Adult'):-
    Age<65,!.
parse_age(_, 'Elderly'):-!.

count_final_weight([]):-!.
count_final_weight([(FinalWeight, Salary)|Next]):-
    Salary='<=50K',
    parse_final_weight(FinalWeight, CategorizedFinalWeight),
    exist_check(CategorizedFinalWeight),
    counter_less(CategorizedFinalWeight, Count),
    retractall(counter_less(CategorizedFinalWeight, Count)),
    NewCount is Count+1,
    assertz(counter_less(CategorizedFinalWeight,NewCount)),
    count_final_weight(Next).
count_final_weight([(FinalWeight, Salary)|Next]):-
    Salary='>50K',
    parse_final_weight(FinalWeight, CategorizedFinalWeight),
    exist_check(CategorizedFinalWeight),
    counter_more(CategorizedFinalWeight, Count),
    retractall(counter_more(CategorizedFinalWeight, Count)),
    NewCount is Count+1,
    assertz(counter_more(CategorizedFinalWeight,NewCount)),
    count_final_weight(Next).

parse_final_weight(FinalWeight, 'Very Low Final Weight'):-
    FinalWeight<106648,!.
parse_final_weight(FinalWeight, 'Low Final Weight'):-
    FinalWeight<158660,!.
parse_final_weight(FinalWeight, 'Moderate Final Weight'):-
    FinalWeight<196338,!.
parse_final_weight(FinalWeight, 'High Final Weight'):-
    FinalWeight<259878,!.
parse_final_weight(_, 'Very High Final Weight'):-!.

count_func_hours_per_week([]):-!.
count_func_hours_per_week([(HoursPerWeek, Salary)|Next]):-
    Salary='<=50K',
    parse_hours_per_week(HoursPerWeek, CategorizedHoursPerWeek),
    exist_check(CategorizedHoursPerWeek),
    counter_less(CategorizedHoursPerWeek, Count),
    retractall(counter_less(CategorizedHoursPerWeek, Count)),
    NewCount is Count+1,
    assertz(counter_less(CategorizedHoursPerWeek,NewCount)),
    count_func_hours_per_week(Next).
count_func_hours_per_week([(HoursPerWeek, Salary)|Next]):-
    Salary='>50K',
    parse_hours_per_week(HoursPerWeek, CategorizedHoursPerWeek),
    exist_check(CategorizedHoursPerWeek),
    counter_more(CategorizedHoursPerWeek, Count),
    retractall(counter_more(CategorizedHoursPerWeek, Count)),
    NewCount is Count+1,
    assertz(counter_more(CategorizedHoursPerWeek,NewCount)),
    count_func_hours_per_week(Next).

parse_hours_per_week(HoursPerWeek, 'Part-time Worker'):-
    HoursPerWeek<40,!.
parse_hours_per_week(HoursPerWeek, 'Full-time Worker'):-
    HoursPerWeek>40,!.
parse_hours_per_week(_, 'Overworked'):-!.

count_capital_gain([]):-!.
count_capital_gain([(CapitalGain, Salary)|Next]):-
    Salary='<=50K',
    parse_capital_gain(CapitalGain, CategorizedCapitalGain),
    exist_check(CategorizedCapitalGain),
    counter_less(CategorizedCapitalGain, Count),
    retractall(counter_less(CategorizedCapitalGain, Count)),
    NewCount is Count+1,
    assertz(counter_less(CategorizedCapitalGain,NewCount)),
    count_capital_gain(Next).
count_capital_gain([(CapitalGain, Salary)|Next]):-
    Salary='>50K',
    parse_capital_gain(CapitalGain, CategorizedCapitalGain),
    exist_check(CategorizedCapitalGain),
    counter_more(CategorizedCapitalGain, Count),
    retractall(counter_more(CategorizedCapitalGain, Count)),
    NewCount is Count+1,
    assertz(counter_more(CategorizedCapitalGain,NewCount)),
    count_capital_gain(Next).

parse_capital_gain(CapitalGain, 'Have Capital Gain'):-
    CapitalGain>0,!.
parse_capital_gain(_, 'No Capital Gain'):-!.

count_capital_loss([]):-!.
count_capital_loss([(CapitalLoss, Salary)|Next]):-
    Salary='<=50K',
    parse_capital_loss(CapitalLoss, CategorizedCapitalLoss),
    exist_check(CategorizedCapitalLoss),
    counter_less(CategorizedCapitalLoss, Count),
    retractall(counter_less(CategorizedCapitalLoss, Count)),
    NewCount is Count+1,
    assertz(counter_less(CategorizedCapitalLoss,NewCount)),
    count_capital_loss(Next).
count_capital_loss([(CapitalLoss, Salary)|Next]):-
    Salary='>50K',
    parse_capital_loss(CapitalLoss, CategorizedCapitalLoss),
    exist_check(CategorizedCapitalLoss),
    counter_more(CategorizedCapitalLoss, Count),
    retractall(counter_more(CategorizedCapitalLoss, Count)),
    NewCount is Count+1,
    assertz(counter_more(CategorizedCapitalLoss,NewCount)),
    count_capital_loss(Next).

parse_capital_loss(CapitalLoss, 'Have Capital Loss'):-
    CapitalLoss>0,!.
parse_capital_loss(_, 'No Capital Loss'):-!.

naive_bayes(List, Result, Confidence):-
    counter_more('Total', TotalMore),
    counter_less('Total', TotalLess),

    mult_more(List, ProbMoreTmp),
    mult_less(List, ProbLessTmp),

    ProbMore is ProbMoreTmp*(TotalMore/(TotalMore+TotalLess)),
    ProbLess is ProbLessTmp*(TotalLess/(TotalMore+TotalLess)),


    more_or_less('>50K', '<=50K', ProbMore, ProbLess, Result,Confidence),
    !.

mult_more([], 1):-!.
mult_more([Attr|Next], Ans):- 
    counter_more(Attr, Chance),
    counter_more('Total', Total),
    mult_more(Next, TempAns),
    Ans is (Chance/Total)*TempAns.

mult_less([], 1):-!.
mult_less([Attr|Next], Ans):- 
    counter_less(Attr, Chance),
    counter_less('Total', Total),
    mult_less(Next, TempAns),
    Ans is (Chance/Total)*TempAns.

more_or_less(MoreLabel, _, Value1, Value2, MoreLabel, Confidence):- 
    Value1>Value2,
    Confidence is Value1/(Value1+Value2).

more_or_less(_, LessLabel, Value1, Value2, LessLabel, Confidence):- 
    Value1<Value2,
    Confidence is Value2/(Value1+Value2).



evaluation_metric(Tp,Tn,Fp,Fn) :-
    findall((Age,FinalWeight,WorkClass,Education,MaritalStatus,Occupation,Relationship,Race,Sex,CapitalGain,CapitalLoss,HoursPerWeek,NativeCountry,Salary),
        fact_testing(Age,
            WorkClass,
            FinalWeight,
            Education,
            _,
            MaritalStatus,
            Occupation,
            Relationship,
            Race,
            Sex,
            CapitalGain,
            CapitalLoss,
            HoursPerWeek,
            NativeCountry,
            Salary),
            EvaluationList),
    evaluation_metric(EvaluationList,0,0,0,0,Tp,Tn,Fp,Fn),
    show_evaluation(Tp,Tn,Fp,Fn),!.


evaluation_metric([],Tp,Tn,Fp,Fn,Tp,Tn,Fp,Fn).

evaluation_metric([(Age,FinalWeight,WorkClass,Education,MaritalStatus,Occupation,Relationship,Race,Sex,CapitalGain,CapitalLoss,HoursPerWeek,NativeCountry,Salary)|Next],
    Ptp,Ptn,Pfp,Pfn,Tp,Tn,Fp,Fn) :-
    parse_final_weight(FinalWeight,ParsedFinalWeight),
    parse_age(Age, ParsedAge),
    parse_capital_gain(CapitalGain, ParsedCapitalGain),
    parse_capital_loss(CapitalLoss, ParsedCapitalLoss),
    parse_hours_per_week(HoursPerWeek, ParsedHoursPerWeek),
    naive_bayes([ParsedAge,ParsedFinalWeight,WorkClass,Education,MaritalStatus,Occupation,Relationship,Race,Sex,ParsedCapitalGain,ParsedCapitalLoss,ParsedHoursPerWeek,NativeCountry],Result,_),
    Result=='>50K',
    Salary=='>50K',
    NewPtp is Ptp+1,
    evaluation_metric(Next,NewPtp,Ptn,Pfp,Pfn,Tp,Tn,Fp,Fn).

evaluation_metric([(Age,FinalWeight,WorkClass,Education,MaritalStatus,Occupation,Relationship,Race,Sex,CapitalGain,CapitalLoss,HoursPerWeek,NativeCountry,Salary)|Next],
    Ptp,Ptn,Pfp,Pfn,Tp,Tn,Fp,Fn) :-
    parse_final_weight(FinalWeight,ParsedFinalWeight),
    parse_age(Age, ParsedAge),
    parse_capital_gain(CapitalGain, ParsedCapitalGain),
    parse_capital_loss(CapitalLoss, ParsedCapitalLoss),
    parse_hours_per_week(HoursPerWeek, ParsedHoursPerWeek),
    naive_bayes([ParsedAge,ParsedFinalWeight,WorkClass,Education,MaritalStatus,Occupation,Relationship,Race,Sex,ParsedCapitalGain,ParsedCapitalLoss,ParsedHoursPerWeek,NativeCountry],Result,_),
    Result=='<=50K',
    Salary=='<=50K',
    NewPtn is Ptn+1,
    evaluation_metric(Next,Ptp,NewPtn,Pfp,Pfn,Tp,Tn,Fp,Fn).

evaluation_metric([(Age,FinalWeight,WorkClass,Education,MaritalStatus,Occupation,Relationship,Race,Sex,CapitalGain,CapitalLoss,HoursPerWeek,NativeCountry,Salary)|Next],
    Ptp,Ptn,Pfp,Pfn,Tp,Tn,Fp,Fn) :-
    parse_final_weight(FinalWeight,ParsedFinalWeight),
    parse_age(Age, ParsedAge),
    parse_capital_gain(CapitalGain, ParsedCapitalGain),
    parse_capital_loss(CapitalLoss, ParsedCapitalLoss),
    parse_hours_per_week(HoursPerWeek, ParsedHoursPerWeek),
    naive_bayes([ParsedAge,ParsedFinalWeight,WorkClass,Education,MaritalStatus,Occupation,Relationship,Race,Sex,ParsedCapitalGain,ParsedCapitalLoss,ParsedHoursPerWeek,NativeCountry],Result,_),
    Result=='>50K',
    Salary=='<=50K',
    NewPfp is Pfp+1,
    evaluation_metric(Next,Ptp,Ptn,NewPfp,Pfn,Tp,Tn,Fp,Fn).

evaluation_metric([(Age,FinalWeight,WorkClass,Education,MaritalStatus,Occupation,Relationship,Race,Sex,CapitalGain,CapitalLoss,HoursPerWeek,NativeCountry,Salary)|Next],
    Ptp,Ptn,Pfp,Pfn,Tp,Tn,Fp,Fn) :-
    parse_final_weight(FinalWeight,ParsedFinalWeight),
    parse_age(Age, ParsedAge),
    parse_capital_gain(CapitalGain, ParsedCapitalGain),
    parse_capital_loss(CapitalLoss, ParsedCapitalLoss),
    parse_hours_per_week(HoursPerWeek, ParsedHoursPerWeek),
    naive_bayes([ParsedAge,ParsedFinalWeight,WorkClass,Education,MaritalStatus,Occupation,Relationship,Race,Sex,ParsedCapitalGain,ParsedCapitalLoss,ParsedHoursPerWeek,NativeCountry],Result,_),
    Result=='<=50K',
    Salary=='>50K',
    NewPfn is Pfn+1,
    evaluation_metric(Next,Ptp,Ptn,Pfp,NewPfn,Tp,Tn,Fp,Fn).


show_evaluation(TP,TN,FP,FN) :-
    Accuracy is (TP+TN) / (TP+FP+FN+TN),
    Precision is TP / (TP+FP),
    Recall is TP / (TP+FN),
    F1 is (2*Precision*Recall) / (Precision+Recall),
    Specificity is TN / (TN+FP),
    write("Accuracy: "), writeln(Accuracy),
    write("Precision: "), writeln(Precision),
    write("Recall: "), writeln(Recall),
    write("F1-score: "), writeln(F1),
    write("Specificity: "), writeln(Specificity).




